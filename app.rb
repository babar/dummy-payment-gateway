require 'sinatra/base'
require 'sinatra/json'
require 'data_mapper'

DataMapper::setup(:default, "sqlite3://#{Dir.pwd}/transactions.db")

class Transaction
  include DataMapper::Resource

  property :id, Serial
  property :amount, Float
  property :card_no, Integer
  property :created_at, DateTime
end

DataMapper.finalize
Transaction.auto_upgrade!

class App < Sinatra::Base
  use Rack::Auth::Basic, "Protected Area" do |u, p|
    u == 'Hello' && p == 'Mello'
  end
  
  post '/transactions' do
    if (Transaction.last && Transaction.last.id % 10 == 0) || params[:card_no].empty?
      output = {success: false, error_message: 'Sorry, your card could not be charged.'}
    else
      Transaction.create amount: params[:amount], card_no: params[:card_no]
      output = {success: true, message: 'Transaction Successful'}
    end

    json output
  end
end
