set :user, 'Babar'

server 'dime.ibabar.com', user: fetch(:user), roles: %w{web app db}, ssh_options: { port: 23554 }

set :application, 'dummy-payment-gateway'
set :repo_url, 'https://gitlab.com/babar/dummy-payment-gateway.git'

set :deploy_to, "/home/#{fetch(:user)}/Sites/#{fetch(:application)}"

set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, File.read('.ruby-version').strip.sub('ruby-', '')
set :rbenv_path, '$HOME/.rbenv'

set :linked_files, fetch(:linked_files, []).push('transactions.db')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/sockets')

set :log_level, :info

set :keep_releases, 3

namespace :deploy do
  def template(from, to = shared_path)
    template_path = "config/#{from}.erb"
    template = ERB.new(File.new(template_path).read).result(binding)
    upload! StringIO.new(template), "#{to}/#{from}"
  end

  desc 'Upload NginX Config'
  task :nginx_conf do
    on roles(:web) do |role|
      template 'nginx.conf'
      execute :sudo, 'service nginx restart'
    end
  end
end
